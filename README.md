# README #

Web server is hosted on cloud.

In order to acces site please visit [1](http://20.106.178.219/).

## GET 

[http://20.106.178.219/users](http://20.106.178.219/users)

## SSL Termination 

```nginx
server {
    listen 80;
    ssl on;
    ssl_certificate /ssl_proxy/cert.pem;
    ssl_certificate_key /ssl_proxy/key.pem;

    location / {

        proxy_pass http://127.0.0.1:3002;

    }

}
```
